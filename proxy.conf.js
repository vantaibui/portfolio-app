const PROXY_CONFIG = [
    {
        context: "/api/product",
        target: "http://localhost:3000",
        secure: true,
        changeOrigin: true,
        logLevel: "debug",
        cookieDomainRewrite: "locahost",
        followRedirects: true,
    }
];

module.exports = PROXY_CONFIG;