import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PortfolioDashboardRoutingModule } from './portfolio-dashboard-routing.module';
import { PortfolioDashboardService } from './portfolio-dashboard.service';
import { PortfolioLayoutComponent } from './portfolio-layout/portfolio-layout.component';


@NgModule({
  declarations: [
    PortfolioLayoutComponent
  ],
  imports: [
    CommonModule,
    PortfolioDashboardRoutingModule
  ],
  providers: [
    PortfolioDashboardService
  ]
})
export class PortfolioDashboardModule { }
