import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrganicFoodLayoutComponent } from './organic-food-layout/organic-food-layout.component';

const routes: Routes = [
  { path: "", component: OrganicFoodLayoutComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganicFoodDashboardRoutingModule { }
