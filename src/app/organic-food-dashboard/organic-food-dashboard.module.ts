import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrganicFoodDashboardRoutingModule } from './organic-food-dashboard-routing.module';
import { OrganicFoodLayoutComponent } from './organic-food-layout/organic-food-layout.component';


@NgModule({
  declarations: [
    OrganicFoodLayoutComponent
  ],
  imports: [
    CommonModule,
    OrganicFoodDashboardRoutingModule
  ]
})
export class OrganicFoodDashboardModule { }
