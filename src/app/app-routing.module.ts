import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: "", redirectTo: "/portfolio", pathMatch: "full" },
  {
    path: "portfolio", loadChildren: () =>
      import("./portfolio-dashboard/portfolio-dashboard.module")
        .then((m) => m.PortfolioDashboardModule)
        .catch(() => "abc")
  },
  {
    path: "organic-food", loadChildren: () =>
      import("./organic-food-dashboard/organic-food-dashboard.module")
        .then((m) => m.OrganicFoodDashboardModule)
        .catch(() => "abc")
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
